#include "DPDUtils/ZeeFilter.h"
#include "DPDUtils/DPDPhotonFilter.h"
#include "DPDUtils/DPDTagTool.h"
#include "DPDUtils/ObjectThinner.h"

DECLARE_COMPONENT( DPDPhotonFilter )
DECLARE_COMPONENT( ZeeFilter )
DECLARE_COMPONENT( ObjectThinner )

DECLARE_COMPONENT( DPDTagTool )

